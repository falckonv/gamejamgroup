# README #

### Setup ###
Download the project and open it in Unity. The game executable is in the Builds folder.

### Game objective ###
Escape the mansion by finding hidden switches that open secret doors.    
To find the switches, the player needs to solve puzzles, destroy crates, and complete platforming challenges.    
The game is won when the player is outside.  

### Controls ###
Move with WASD or the arrow keys   
Jump with the Space bar    
Attack by pressing Enter  

### Sources ###
Unity animations: https://www.youtube.com/watch?v=sgHicuJAu3g