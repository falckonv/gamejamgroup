﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarpetController : MonoBehaviour
{

    public GameObject player;
    public GameObject target;  // Secret door?

    private bool isInRoom;
    private float distanceToCarpet;
    private float speed = 4f;
    private float rotationSpeed = 100f;

    private bool isAutoPilot = false;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    float calculateDistance(GameObject target)
    {
        float distUnity = Vector3.Distance(transform.position, target.transform.position);
        return distUnity;
    }

    bool isOnTop()
    {
        // Check if player has position equal to carpets height
        distanceToCarpet = calculateDistance(player);
        if (distanceToCarpet < 6f) 
        {
            isAutoPilot = false;
            return true;
        }
        return false;
    }



    // Update is called once per frame
    void Update()
    {
        distanceToCarpet = calculateDistance(player);

        if (distanceToCarpet < 30f)
        {
            isInRoom = true;
            isAutoPilot = true;

            if (isOnTop())
            {
                // The capet will fly the ghost to the target
                // TODO: animate flying carpet + sound 
                isAutoPilot = false;
                //transform.position = new Vector3(player.transform.position.x, 6, player.transform.position.z);

            }

            if (isAutoPilot) {
                // The carpet will follow the player until player is close enough to jump on
                Vector3 lookAtPlayer = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                Vector3 directionToPlayer = lookAtPlayer - transform.position;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(directionToPlayer), Time.deltaTime * rotationSpeed);
                //transform.Translate(0,0, speed * Time.deltaTime);
            }
        }
        else {
            isInRoom = false; 
        }
    }
}
