﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowMeCameraController : MonoBehaviour
{
    public GameObject player;  // Choose the GameObject to be followed by camera
    private Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player) {
            transform.position = player.transform.position + offset;
        }
        else Debug.Log("Player not recognized by CameraController");
    } 
}
