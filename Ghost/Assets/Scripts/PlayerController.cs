﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private Rigidbody playerRb;
    private Animator playerAnim;

    public GameObject winScreen;

    private GameObject crateDestruction;

    private Vector3 startPosition = new Vector3(8, 0, -35);

    private float verticalInput;
    private float horizontalInput;
    private float speed = 10;
    private float jumpForce = 0.75f;

    private float raycastOnGroundDistance = 0.2f; // Minimum height from ground colliders to consider player to be on the ground
    private bool isOnGround;
    private float minYPos = -5;          // Minimum allowed Y position for player

    private bool attackCooldown;         // To avoid attack spam
    private float raycastForwardDistance = 2;

    private bool gameRunning;

    // Start is called before the first frame update
    void Start()
    {
        playerRb = GetComponent<Rigidbody>();
        playerAnim = GetComponent<Animator>();

        winScreen.SetActive(false);

        crateDestruction = GameObject.Find("Crate Destruction");

        isOnGround = true;
        attackCooldown = false;

        gameRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        // Reset position if out of bounds
        if (transform.position.y <= minYPos)
        {
            transform.position = startPosition;
        }

        if (gameRunning)
        {
            XZMovement();
            JumpControls();
            AttackControls();
        }
    }

    private void XZMovement()
    {
        verticalInput = Input.GetAxisRaw("Vertical");
        horizontalInput = Input.GetAxisRaw("Horizontal");

        Vector3 inputVector = new Vector3(horizontalInput, 0, verticalInput);

        // Rotates player in the direction they are going if there is XZ movement input
        if (inputVector != Vector3.zero)
        {
            playerRb.rotation = Quaternion.Slerp(playerRb.rotation, Quaternion.LookRotation(inputVector), 0.15f);
        }

        Vector3 movement = inputVector * speed * Time.deltaTime;
        playerRb.MovePosition(transform.position + movement);
    }

    private void JumpControls()
    {
        // Checks if player is standing on the ground
        if (!Physics.Raycast(transform.position, Vector3.down, raycastOnGroundDistance))
        {
            isOnGround = false;
        }
        else
        {
            isOnGround = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && isOnGround)
        {
            playerRb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
            isOnGround = false;
        }
    }

    private void AttackControls()
    {
        if (Input.GetKeyDown(KeyCode.Return) && !attackCooldown)
        {
            playerAnim.SetTrigger("Attack");

            // Starts cooldown and stops cooldown after a time
            attackCooldown = true;
            Invoke("ResetCooldown", 0.5f);

            // Send raycast forward. If the ray hits a crate, the crate is destroyed
            RaycastHit hit;
            Vector3 raycastOrigin = transform.position + new Vector3(0, 2, 0);
            if (Physics.Raycast(raycastOrigin, transform.forward, out hit, raycastForwardDistance))
            {
                GameObject hitObject = hit.collider.gameObject;
                if (hitObject.CompareTag("Crate"))
                {
                    crateDestruction.transform.position = 
                        hitObject.transform.position + new Vector3(1.5f, 1.5f, -1.5f);
                    Destroy(hitObject);
                    crateDestruction.GetComponent<ParticleSystem>().Play();
                }
            }
        }
    }

    private void ResetCooldown()
    {
        attackCooldown = false;
    }

    // Restart game by reloading the current scene
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Outside"))
        {
            winScreen.SetActive(true);
            gameRunning = false;
        }
    }
}
