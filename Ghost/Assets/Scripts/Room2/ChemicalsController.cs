﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChemicalsController : MonoBehaviour
{
    public GameObject player;  // Target for the TestTube
    public GameObject enzyme;
    public GameObject enzyme1;
    
    public float speed = 4f;
    public float rotationSpeed = 100.0f;
    
    private float distanceToPlayer;
    private float distanceToEnzyme;
    private bool isInRoom       = false;

  
    // Start is called before the first frame update
    void Start()
    {
    }

    float calculateDistance(GameObject target)
    {
        float distUnity = Vector3.Distance(transform.position, target.transform.position);
        return distUnity;
    }

    void checkForEnzyme(GameObject enzyme)
    {
        distanceToEnzyme = calculateDistance(enzyme);
        if (distanceToEnzyme < 2f)
        {
            Destroy(this.gameObject, 3);
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        distanceToPlayer = calculateDistance(player);

        checkForEnzyme(enzyme);
        checkForEnzyme(enzyme1);

    
        if (distanceToPlayer < 30f)
        {
            //Debug.Log("Distance is closer than 30f");
            isInRoom = true;
            if (isInRoom) {
                // TODO: make TestTubes sparkle - sound or light or color change
            }

            if (distanceToPlayer < 10f)
            {
                // The chemicals will follow the player
                Vector3 lookAtTarget = new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z);
                Vector3 direction = lookAtTarget - transform.position;
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);
                transform.Translate(0,0, speed * Time.deltaTime);
            }
        }

        else isInRoom = false; 
    }
}
