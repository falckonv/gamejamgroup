﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChemicalDoorOpener : MonoBehaviour
{

    public GameObject secretDoorModel;  // Deal with the open door here!
    private Animation secretDoorOpen;   // Related to open the door
    private int numberOfTestTubes = 4;
    private int counter = 0;
    
    // Start is called before the first frame update
    void Start()
    {
         secretDoorOpen = secretDoorModel.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {

        // TODO: find out how to count number of Destroy TestTubes versus total
        // number of TestTubes on the shelf
        // Could invoke new TestTube objects from this script and destroy them here!
        // Need a closer look to get this to work
         if (counter >= numberOfTestTubes)  // The criteria for opening the secret door
        {
            secretDoorOpen.Play();
        }
        
    }
}
