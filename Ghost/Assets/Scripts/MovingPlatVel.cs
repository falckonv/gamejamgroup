﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatVel : MonoBehaviour
{
    public Vector3 v3Force;

    // Update is called once per frame
    void FixedUpdate()
    {
        GetComponent<Rigidbody>().velocity += v3Force;
    }
}
