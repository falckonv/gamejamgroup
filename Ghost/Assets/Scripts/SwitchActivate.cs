﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchActivate : MonoBehaviour
{
    private MeshRenderer meshRenderer;
    public Material switchGreen;
    private Animation pressAnimation;

    private bool switchPressed;

    public GameObject secretDoorModel;
    private Animation secretDoorOpenAnim;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = gameObject.GetComponent<MeshRenderer>();
        pressAnimation = GetComponent<Animation>();

        switchPressed = false;

        secretDoorOpenAnim = secretDoorModel.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && !switchPressed)
        {
            meshRenderer.material = switchGreen;
            pressAnimation.Play();
            switchPressed = true;

            secretDoorOpenAnim.Play();
        }
    }
}
